﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugTracker.Classes;
using System.Collections.Generic;

namespace BugTrackerTests
{
    [TestClass]
    public class BugReportTests
    {

        /// <summary>
        /// To test Bugreport entity and its relational classes
        /// </summary>
        [TestMethod]
        public void BugReportEntityTest()
        {

            BugReportAudit reportAudit = new BugReportAudit();
            reportAudit.AuditDate = new DateTime(2016, 12, 23, 09, 00, 00);
            reportAudit.Comment = "Test Comment";
            

            BugReport bugReport = new BugReport();
            bugReport.BugReportId = 131;
            bugReport.Title = "Test Title";
            bugReport.BugReportAudits.Add(reportAudit);

            List<BugReport> listBugReport = new List<BugReport>();
            listBugReport.Add(bugReport);

            var data = listBugReport;

            Assert.AreEqual(1, data[0].BugReportAudits.Count);
            Assert.AreEqual("Test Title", data[0].Title);
            Assert.AreEqual(131, data[0].BugReportId);
        }
    }
}
