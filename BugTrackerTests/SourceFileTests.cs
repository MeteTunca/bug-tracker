﻿using System;
using System.Collections.Generic;
using BugTracker.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BugTrackerTests
{
    [TestClass]
    public class SourceFileTests
    {

        /// <summary>
        /// To test sourcefile entity and its relational classes
        /// </summary>
        [TestMethod]
        public void SourceFileEntityTest()
        {
            //UserType uType=new UserType();
            //uType.Type = "Developer";
            //uType.UserTypeId = 2;
        
            //User user=new User();
            //user.Firstname = "Kevin";
            //user.Lastname = "Durant";
            //user.UserType = uType;
            //user.UserTypeId = uType.UserTypeId;

            BugReport bugReport = new BugReport();
            bugReport.BugReportId = 131;
            bugReport.Title = "Test Title";

            SourceFile sFile=new SourceFile();
            sFile.FileName = "Person.cs";
            sFile.DownloadUrl = "http://www.github.Metetunca/BugTrackerrepo/person/cs";
            sFile.BugReports.Add(bugReport);

            List<SourceFile> listSourceFile = new List<SourceFile>();
            listSourceFile.Add(sFile);

            var data = listSourceFile;

            Assert.AreEqual("http://www.github.Metetunca/BugTrackerrepo/person/cs", data[0].DownloadUrl);
            Assert.AreEqual("Person.cs", data[0].FileName);
            Assert.AreEqual(1, data[0].BugReports.Count);
        }
    }
}
