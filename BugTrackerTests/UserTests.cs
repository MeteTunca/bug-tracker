﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugTracker.Classes;
using System.Collections.Generic;

namespace BugTrackerTests
{
    [TestClass]
    public class UserTests
    {

        /// <summary>
        /// To test user entity and its relational classes
        /// </summary>
        [TestMethod]
        public void UserEntityTest()
        {
            UserType uType = new UserType();
            uType.Type = "Developer";
            uType.UserTypeId = 2;

            SourceFile sFile = new SourceFile();
            sFile.FileName = "Person.cs";

            User user = new User();
            user.Firstname = "Kevin";
            user.Lastname = "Durant";
            user.UserType = uType;
            user.UserTypeId = uType.UserTypeId;
            user.SourceFiles.Add(sFile);

            List<User> listUser = new List<User>();
            listUser.Add(user);

            var data = listUser;

            Assert.AreEqual(2, data[0].UserTypeId);
            Assert.AreEqual("Durant", data[0].Lastname);
            Assert.AreEqual(1, data[0].SourceFiles.Count);
            
        }
    }
}
