﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugTracker.Classes;
using System.Collections.Generic;

namespace BugTrackerTests
{
    [TestClass]
    public class BugReportAuditTests
    {

        /// <summary>
        /// To test bugreportaudit entity and its relational classes
        /// </summary>
        [TestMethod]
        public void BugReportAuditEntityTest()
        {

            User testUser = new User();
            testUser.Firstname = "Mete";

            ReportState state = new ReportState();
            state.StateId = 1;

            BugReportAudit reportAudit = new BugReportAudit();
            reportAudit.AuditDate = new DateTime(2016, 12, 23, 09, 00, 00);
            reportAudit.Comment = "Test Comment";
            reportAudit.User = testUser;
            reportAudit.ReportState = state;
            reportAudit.BugStateId = state.StateId;

            List<BugReportAudit> listReportAudit = new List<BugReportAudit>();
            listReportAudit.Add(reportAudit);

            var data = listReportAudit;

            Assert.AreEqual(new DateTime(2016, 12, 23, 09, 00, 00), data[0].AuditDate);
            Assert.AreEqual("Test Comment", data[0].Comment);
            Assert.AreEqual("Mete", data[0].User.Firstname);
        }
    }
}
