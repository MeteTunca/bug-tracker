﻿namespace BugTracker
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxComment = new System.Windows.Forms.CheckBox();
            this.comboBoxUser = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxReportStatus = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxCodeAuthor = new System.Windows.Forms.ComboBox();
            this.textBoxLineNumberLast = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxLineNumberfirst = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxTitle = new System.Windows.Forms.TextBox();
            this.textBoxSourceFile = new System.Windows.Forms.TextBox();
            this.buttonUpdateBug = new System.Windows.Forms.Button();
            this.buttonAddBug = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.buttonClearForm = new System.Windows.Forms.Button();
            this.listBoxBugAudit = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxProjectName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxClassName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxMethodName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxBugComment = new System.Windows.Forms.TextBox();
            this.listBoxBugList = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.buttonGoRepositoryOnline = new System.Windows.Forms.Button();
            this.labelConnStatus = new System.Windows.Forms.Label();
            this.buttonDownloadSelectedFile = new System.Windows.Forms.Button();
            this.buttonGoSourceFileOnline = new System.Windows.Forms.Button();
            this.listBoxGitHubRepo = new System.Windows.Forms.ListBox();
            this.buttonConnectGitHub = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(924, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxComment);
            this.groupBox3.Controls.Add(this.comboBoxUser);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.comboBoxReportStatus);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.comboBoxCodeAuthor);
            this.groupBox3.Controls.Add(this.textBoxLineNumberLast);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.textBoxLineNumberfirst);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.textBoxTitle);
            this.groupBox3.Controls.Add(this.textBoxSourceFile);
            this.groupBox3.Controls.Add(this.buttonUpdateBug);
            this.groupBox3.Controls.Add(this.buttonAddBug);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.buttonClearForm);
            this.groupBox3.Controls.Add(this.listBoxBugAudit);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.textBoxProjectName);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.textBoxClassName);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.textBoxMethodName);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.textBoxBugComment);
            this.groupBox3.Location = new System.Drawing.Point(296, 29);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(617, 340);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Bug Details";
            // 
            // checkBoxComment
            // 
            this.checkBoxComment.AutoSize = true;
            this.checkBoxComment.Location = new System.Drawing.Point(219, 194);
            this.checkBoxComment.Name = "checkBoxComment";
            this.checkBoxComment.Size = new System.Drawing.Size(92, 17);
            this.checkBoxComment.TabIndex = 44;
            this.checkBoxComment.Text = "Add Comment";
            this.checkBoxComment.UseVisualStyleBackColor = true;
            this.checkBoxComment.CheckedChanged += new System.EventHandler(this.checkBoxComment_CheckedChanged);
            // 
            // comboBoxUser
            // 
            this.comboBoxUser.FormattingEnabled = true;
            this.comboBoxUser.Location = new System.Drawing.Point(89, 244);
            this.comboBoxUser.Name = "comboBoxUser";
            this.comboBoxUser.Size = new System.Drawing.Size(111, 21);
            this.comboBoxUser.TabIndex = 43;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 247);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 42;
            this.label12.Text = "Last User:";
            // 
            // comboBoxReportStatus
            // 
            this.comboBoxReportStatus.FormattingEnabled = true;
            this.comboBoxReportStatus.Location = new System.Drawing.Point(89, 217);
            this.comboBoxReportStatus.Name = "comboBoxReportStatus";
            this.comboBoxReportStatus.Size = new System.Drawing.Size(111, 21);
            this.comboBoxReportStatus.TabIndex = 41;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 221);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Bug Status:";
            // 
            // comboBoxCodeAuthor
            // 
            this.comboBoxCodeAuthor.FormattingEnabled = true;
            this.comboBoxCodeAuthor.Location = new System.Drawing.Point(89, 190);
            this.comboBoxCodeAuthor.Name = "comboBoxCodeAuthor";
            this.comboBoxCodeAuthor.Size = new System.Drawing.Size(111, 21);
            this.comboBoxCodeAuthor.TabIndex = 39;
            // 
            // textBoxLineNumberLast
            // 
            this.textBoxLineNumberLast.Location = new System.Drawing.Point(157, 58);
            this.textBoxLineNumberLast.Name = "textBoxLineNumberLast";
            this.textBoxLineNumberLast.Size = new System.Drawing.Size(43, 20);
            this.textBoxLineNumberLast.TabIndex = 36;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(136, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 13);
            this.label10.TabIndex = 38;
            this.label10.Text = "to";
            // 
            // textBoxLineNumberfirst
            // 
            this.textBoxLineNumberfirst.Location = new System.Drawing.Point(89, 58);
            this.textBoxLineNumberfirst.Name = "textBoxLineNumberfirst";
            this.textBoxLineNumberfirst.Size = new System.Drawing.Size(43, 20);
            this.textBoxLineNumberfirst.TabIndex = 35;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 37;
            this.label9.Text = "Lines of Code:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Title:";
            // 
            // textBoxTitle
            // 
            this.textBoxTitle.Location = new System.Drawing.Point(89, 31);
            this.textBoxTitle.Name = "textBoxTitle";
            this.textBoxTitle.Size = new System.Drawing.Size(521, 20);
            this.textBoxTitle.TabIndex = 0;
            // 
            // textBoxSourceFile
            // 
            this.textBoxSourceFile.Location = new System.Drawing.Point(89, 164);
            this.textBoxSourceFile.Name = "textBoxSourceFile";
            this.textBoxSourceFile.Size = new System.Drawing.Size(112, 20);
            this.textBoxSourceFile.TabIndex = 9;
            // 
            // buttonUpdateBug
            // 
            this.buttonUpdateBug.Image = global::BugTracker.Properties.Resources.update_button;
            this.buttonUpdateBug.Location = new System.Drawing.Point(78, 269);
            this.buttonUpdateBug.Name = "buttonUpdateBug";
            this.buttonUpdateBug.Size = new System.Drawing.Size(54, 56);
            this.buttonUpdateBug.TabIndex = 14;
            this.buttonUpdateBug.UseVisualStyleBackColor = true;
            this.buttonUpdateBug.Click += new System.EventHandler(this.buttonUpdateBug_Click);
            // 
            // buttonAddBug
            // 
            this.buttonAddBug.Image = global::BugTracker.Properties.Resources.new_button;
            this.buttonAddBug.Location = new System.Drawing.Point(14, 269);
            this.buttonAddBug.Name = "buttonAddBug";
            this.buttonAddBug.Size = new System.Drawing.Size(55, 56);
            this.buttonAddBug.TabIndex = 13;
            this.buttonAddBug.UseVisualStyleBackColor = true;
            this.buttonAddBug.Click += new System.EventHandler(this.buttonAddBug_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(216, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Bug Audit:";
            // 
            // buttonClearForm
            // 
            this.buttonClearForm.Image = global::BugTracker.Properties.Resources.clear_button2;
            this.buttonClearForm.Location = new System.Drawing.Point(139, 269);
            this.buttonClearForm.Name = "buttonClearForm";
            this.buttonClearForm.Size = new System.Drawing.Size(57, 56);
            this.buttonClearForm.TabIndex = 15;
            this.buttonClearForm.UseVisualStyleBackColor = true;
            this.buttonClearForm.Click += new System.EventHandler(this.buttonClearForm_Click);
            // 
            // listBoxBugAudit
            // 
            this.listBoxBugAudit.FormattingEnabled = true;
            this.listBoxBugAudit.Location = new System.Drawing.Point(219, 77);
            this.listBoxBugAudit.Name = "listBoxBugAudit";
            this.listBoxBugAudit.Size = new System.Drawing.Size(392, 108);
            this.listBoxBugAudit.TabIndex = 28;
            this.listBoxBugAudit.SelectedIndexChanged += new System.EventHandler(this.listBoxBugAudit_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Code Author:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Source File:";
            // 
            // textBoxProjectName
            // 
            this.textBoxProjectName.Location = new System.Drawing.Point(89, 138);
            this.textBoxProjectName.Name = "textBoxProjectName";
            this.textBoxProjectName.Size = new System.Drawing.Size(112, 20);
            this.textBoxProjectName.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Project Name:";
            // 
            // textBoxClassName
            // 
            this.textBoxClassName.Location = new System.Drawing.Point(89, 112);
            this.textBoxClassName.Name = "textBoxClassName";
            this.textBoxClassName.Size = new System.Drawing.Size(112, 20);
            this.textBoxClassName.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Class Name:";
            // 
            // textBoxMethodName
            // 
            this.textBoxMethodName.Location = new System.Drawing.Point(89, 86);
            this.textBoxMethodName.Name = "textBoxMethodName";
            this.textBoxMethodName.Size = new System.Drawing.Size(112, 20);
            this.textBoxMethodName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Method Name:";
            // 
            // textBoxBugComment
            // 
            this.textBoxBugComment.Location = new System.Drawing.Point(219, 217);
            this.textBoxBugComment.Multiline = true;
            this.textBoxBugComment.Name = "textBoxBugComment";
            this.textBoxBugComment.Size = new System.Drawing.Size(391, 108);
            this.textBoxBugComment.TabIndex = 12;
            // 
            // listBoxBugList
            // 
            this.listBoxBugList.FormattingEnabled = true;
            this.listBoxBugList.HorizontalScrollbar = true;
            this.listBoxBugList.Location = new System.Drawing.Point(6, 19);
            this.listBoxBugList.Name = "listBoxBugList";
            this.listBoxBugList.Size = new System.Drawing.Size(269, 316);
            this.listBoxBugList.TabIndex = 1;
            this.listBoxBugList.SelectedIndexChanged += new System.EventHandler(this.listBoxBugList_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.listBoxBugList);
            this.groupBox4.Location = new System.Drawing.Point(6, 29);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(281, 338);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Bug List";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.buttonGoRepositoryOnline);
            this.groupBox6.Controls.Add(this.labelConnStatus);
            this.groupBox6.Controls.Add(this.buttonDownloadSelectedFile);
            this.groupBox6.Controls.Add(this.buttonGoSourceFileOnline);
            this.groupBox6.Controls.Add(this.listBoxGitHubRepo);
            this.groupBox6.Controls.Add(this.buttonConnectGitHub);
            this.groupBox6.Location = new System.Drawing.Point(6, 369);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(907, 216);
            this.groupBox6.TabIndex = 26;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "GutHub Repository Connection";
            // 
            // buttonGoRepositoryOnline
            // 
            this.buttonGoRepositoryOnline.Image = global::BugTracker.Properties.Resources.linkrepo_button;
            this.buttonGoRepositoryOnline.Location = new System.Drawing.Point(859, 19);
            this.buttonGoRepositoryOnline.Name = "buttonGoRepositoryOnline";
            this.buttonGoRepositoryOnline.Size = new System.Drawing.Size(41, 38);
            this.buttonGoRepositoryOnline.TabIndex = 26;
            this.buttonGoRepositoryOnline.Text = "...";
            this.buttonGoRepositoryOnline.UseVisualStyleBackColor = true;
            this.buttonGoRepositoryOnline.Click += new System.EventHandler(this.buttonGoRepositoryOnline_Click);
            // 
            // labelConnStatus
            // 
            this.labelConnStatus.AutoSize = true;
            this.labelConnStatus.Location = new System.Drawing.Point(540, 43);
            this.labelConnStatus.Name = "labelConnStatus";
            this.labelConnStatus.Size = new System.Drawing.Size(79, 13);
            this.labelConnStatus.TabIndex = 25;
            this.labelConnStatus.Text = "Not Connected";
            // 
            // buttonDownloadSelectedFile
            // 
            this.buttonDownloadSelectedFile.Image = global::BugTracker.Properties.Resources.download_button;
            this.buttonDownloadSelectedFile.Location = new System.Drawing.Point(62, 18);
            this.buttonDownloadSelectedFile.Name = "buttonDownloadSelectedFile";
            this.buttonDownloadSelectedFile.Size = new System.Drawing.Size(41, 38);
            this.buttonDownloadSelectedFile.TabIndex = 24;
            this.buttonDownloadSelectedFile.Text = "...";
            this.buttonDownloadSelectedFile.UseVisualStyleBackColor = true;
            this.buttonDownloadSelectedFile.Click += new System.EventHandler(this.buttonDownloadSelectedFile_Click);
            // 
            // buttonGoSourceFileOnline
            // 
            this.buttonGoSourceFileOnline.Image = global::BugTracker.Properties.Resources.button_link;
            this.buttonGoSourceFileOnline.Location = new System.Drawing.Point(112, 18);
            this.buttonGoSourceFileOnline.Name = "buttonGoSourceFileOnline";
            this.buttonGoSourceFileOnline.Size = new System.Drawing.Size(41, 38);
            this.buttonGoSourceFileOnline.TabIndex = 23;
            this.buttonGoSourceFileOnline.Text = "...";
            this.buttonGoSourceFileOnline.UseVisualStyleBackColor = true;
            this.buttonGoSourceFileOnline.Click += new System.EventHandler(this.buttonGoSourceFileOnline_Click);
            // 
            // listBoxGitHubRepo
            // 
            this.listBoxGitHubRepo.FormattingEnabled = true;
            this.listBoxGitHubRepo.Location = new System.Drawing.Point(9, 62);
            this.listBoxGitHubRepo.Name = "listBoxGitHubRepo";
            this.listBoxGitHubRepo.Size = new System.Drawing.Size(891, 147);
            this.listBoxGitHubRepo.TabIndex = 22;
            this.listBoxGitHubRepo.SelectedIndexChanged += new System.EventHandler(this.listBoxGitHubRepo_SelectedIndexChanged);
            this.listBoxGitHubRepo.DoubleClick += new System.EventHandler(this.listBoxGitHubRepo_DoubleClick);
            // 
            // buttonConnectGitHub
            // 
            this.buttonConnectGitHub.Image = global::BugTracker.Properties.Resources.connect1;
            this.buttonConnectGitHub.Location = new System.Drawing.Point(12, 18);
            this.buttonConnectGitHub.Name = "buttonConnectGitHub";
            this.buttonConnectGitHub.Size = new System.Drawing.Size(41, 38);
            this.buttonConnectGitHub.TabIndex = 21;
            this.buttonConnectGitHub.UseVisualStyleBackColor = true;
            this.buttonConnectGitHub.Click += new System.EventHandler(this.buttonConnectGitHub_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 588);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.menuStrip1);
            this.Name = "MainForm";
            this.Text = "Bug Tracker";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox listBoxBugAudit;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox listBoxBugList;
        private System.Windows.Forms.TextBox textBoxBugComment;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxProjectName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxClassName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxMethodName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button buttonAddBug;
        private System.Windows.Forms.Button buttonClearForm;
        private System.Windows.Forms.Button buttonUpdateBug;
        private System.Windows.Forms.Button buttonConnectGitHub;
        private System.Windows.Forms.ListBox listBoxGitHubRepo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxSourceFile;
        private System.Windows.Forms.Button buttonDownloadSelectedFile;
        private System.Windows.Forms.Button buttonGoSourceFileOnline;
        private System.Windows.Forms.Label labelConnStatus;
        private System.Windows.Forms.Button buttonGoRepositoryOnline;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxTitle;
        private System.Windows.Forms.TextBox textBoxLineNumberLast;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxLineNumberfirst;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxCodeAuthor;
        private System.Windows.Forms.ComboBox comboBoxReportStatus;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxUser;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBoxComment;
    }
}