﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Octokit;
using System.Net;
using BugTracker.Classes;

namespace BugTracker
{
    public partial class MainForm : Form
    {
        private string repoUrl;
        private SourceCodeViewer _formScViewer;
        public MainForm()
        {
            InitializeComponent();
        }

        #region This region contains MainForm Methods
        /// <summary>
        /// Needs to be invoked When Form Loading
        /// </summary>
        private void InitilizeFormDefaults()
        {
            InitializeComponentDefaults();
            LoadButtonTips();
            FillCodeAuthorCombobox();
            FillReportStatusComboBox();
            FillUserCombobox();
        }

        /// <summary>
        /// Lists All Bug Reports on ListBoxBugList
        /// </summary>
        private void ShowBugList()
        {
            
            using (var context = new BugTrackerContext())
            {
                GetBugReports getBugReports = new GetBugReports(context);
                IQueryable<BugListResult> bugsList = getBugReports.GetAllBugReports();
                listBoxBugList.DisplayMember = "Title";
                listBoxBugList.ValueMember = "BugReportId";
                listBoxBugList.DataSource = bugsList.ToList();
            }
        }

        /// <summary>
        /// Setup for Visual Components. Needs to be invoked when Form Loading
        /// </summary>
        /// 
        private void InitializeComponentDefaults()
        {
            checkBoxComment.Checked = true;
        }
        /// <summary>
        /// Fill Selected Bug Details
        /// </summary>
        /// <param name="reportId"></param>
        private void FillSelectedBugDetails(int reportId)
        {
           
            using (var context = new BugTrackerContext())
            {
                GetBugReportDetails reportDetail = new GetBugReportDetails(context);
                IQueryable<BugReportDetails> reportDetails= reportDetail.GetAllBugReports(reportId);
                foreach (var details in reportDetails)
                {
                    textBoxTitle.Text = details.Title;
                    textBoxLineNumberfirst.Text = details.LineOfCodeStart;
                    textBoxLineNumberLast.Text = details.LineOfCodeEnd;
                    textBoxMethodName.Text = details.MethodName;
                    textBoxClassName.Text = details.ClassName;
                    textBoxProjectName.Text = details.ProjectName;
                    textBoxSourceFile.Text = details.SourceFile;
                    comboBoxCodeAuthor.SelectedValue = details.CodeAuthor == 0 ? 0 : details.CodeAuthor;
                    comboBoxReportStatus.SelectedValue = details.Status;
                    comboBoxUser.SelectedValue = details.UserId;

                }
            }
        }

        /// <summary>
        /// Fill Selected Bug Audit List On ListBoxBugAudit
        /// </summary>
        /// <param name="reportId"></param>
        private void FillSelectedBugAuditList(int reportId)
        {
            
            using (var context = new BugTrackerContext())
            {
                GetBugAuditList list = new GetBugAuditList(context);
                IQueryable<BugAuditList> auditList = list.GetAllBugReportAuditLists(reportId);
                listBoxBugAudit.DisplayMember = "DisplayItem";
                listBoxBugAudit.ValueMember = "BugReportAuditId";
                listBoxBugAudit.DataSource = auditList.ToList();
            }
        }

        /// <summary>
        /// Fill Selected Bug Audit Comments
        /// </summary>
        /// <param name="bugReportAuditId"></param>
        private void FillSelectedBugAuditComment(int bugReportAuditId)
        {
            using (var context = new BugTrackerContext())
            {
                GetBugAuditComment auditComment=new GetBugAuditComment(context);

                IQueryable<BugAuditList> comment = auditComment.GetBugAuditComments(bugReportAuditId);
                foreach (var comments in comment)
                {
                    textBoxBugComment.Text = comments.Comment;

                }
            }

        }
        /// <summary>
        /// Create new bug report
        /// </summary>
        private void CreateBugReport()
        {
            CreateBugReport Report = new CreateBugReport();
            if (textBoxSourceFile.Text != string.Empty)
            {
                var sourceFile = new SourceFile();
                sourceFile.FileName = textBoxSourceFile.Text;
                sourceFile.CodeAuthorId = Convert.ToInt64(comboBoxCodeAuthor.SelectedValue);
                Report.SourceFile = sourceFile;
            }
            var bugReport = new BugReport();
            bugReport.Title = textBoxTitle.Text;
            bugReport.ClassName = textBoxClassName.Text;
            bugReport.CreateDate = DateTime.Now;
            bugReport.LineOfCodeStart = textBoxLineNumberfirst.Text;
            bugReport.LineOfCodeEnd = textBoxLineNumberLast.Text;
            bugReport.MethodName = textBoxMethodName.Text;
            bugReport.ProjectName = textBoxProjectName.Text;
            Report.BugReport = bugReport;

            var bugReportAudit = new BugTracker.Classes.BugReportAudit();
            bugReportAudit.BugReport = bugReport;
            bugReportAudit.Comment = textBoxBugComment.Text;
            bugReportAudit.BugStateId = Convert.ToInt32(comboBoxReportStatus.SelectedValue);
            bugReportAudit.UserId = Convert.ToInt32(comboBoxUser.SelectedValue);
            bugReportAudit.AuditDate = DateTime.Now;
            Report.BugReportAudit = bugReportAudit;

            if (Report.Save())
                MessageBox.Show(this, "Bug Report Created Successfully", "Message!", MessageBoxButtons.OK);
            ShowBugList();

        }
        /// <summary>
        /// Update the selected bug report details
        /// </summary>
        private void UpdateBugReportDetails()
        {
            UpdateBugReport report = new UpdateBugReport();

            int bugReportId = Convert.ToInt32(listBoxBugList.SelectedValue);
            var bugReport = new BugReport();
            bugReport.BugReportId = bugReportId;
            bugReport.Title = textBoxTitle.Text;
            bugReport.ClassName = textBoxClassName.Text;
            bugReport.CreateDate = DateTime.Now;
            bugReport.LineOfCodeStart = textBoxLineNumberfirst.Text;
            bugReport.LineOfCodeEnd = textBoxLineNumberLast.Text;
            bugReport.MethodName = textBoxMethodName.Text;
            bugReport.ProjectName = textBoxProjectName.Text;
            report.BugReport = bugReport;
            report.Save();

            if (checkBoxComment.Checked)
                AddNewAudittoBugReport();

            FillSelectedBugAuditList(Convert.ToInt32(listBoxBugList.SelectedValue));
            MessageBox.Show(this, "Bug Report Updated Successfully", "Message!", MessageBoxButtons.OK);
        }
        /// <summary>
        /// Add new audit to selected bug report. It contains comment, status, date and user
        /// </summary>
        private void AddNewAudittoBugReport()
        {
            AddAuditToBugReport audit = new AddAuditToBugReport();

            var bugReportAudit = new BugReportAudit();
            bugReportAudit.BugReportId = Convert.ToInt64(listBoxBugList.SelectedValue);
            bugReportAudit.UserId = Convert.ToInt64(comboBoxUser.SelectedValue);
            bugReportAudit.BugStateId = Convert.ToInt32(comboBoxReportStatus.SelectedValue);
            bugReportAudit.AuditDate = DateTime.Now;
            bugReportAudit.Comment = textBoxBugComment.Text;

            audit.BugReportAudit = bugReportAudit;
            audit.Save();



        }
        /// <summary>
        /// Initialize Button Tips. Needs to be invoked when Form Loading
        /// </summary>
        private void LoadButtonTips()
        {
            System.Windows.Forms.ToolTip ToolTip;
            ToolTip = new System.Windows.Forms.ToolTip();
            ToolTip.SetToolTip(this.buttonAddBug, "Create A New Bug");

            ToolTip = new System.Windows.Forms.ToolTip();
            ToolTip.SetToolTip(this.buttonClearForm, "Clear Form");

            ToolTip = new System.Windows.Forms.ToolTip();
            ToolTip.SetToolTip(this.buttonUpdateBug, "Update Bug");

            ToolTip = new System.Windows.Forms.ToolTip();
            ToolTip.SetToolTip(this.buttonConnectGitHub, "Connect GitHub Repository");

            ToolTip = new System.Windows.Forms.ToolTip();
            ToolTip.SetToolTip(this.buttonDownloadSelectedFile, "Download Selected File");

            ToolTip = new System.Windows.Forms.ToolTip();
            ToolTip.SetToolTip(this.buttonGoSourceFileOnline, "Go Selected File Online");

          
        }
        /// <summary>
        /// Fill Code Author Users to comboBoxCodeAuthor. Needs to be invoked when form loading
        /// </summary>
        private void FillCodeAuthorCombobox()
        {
            UserList userList = new UserList();
            userList.FillUserComboBox(comboBoxCodeAuthor);
        }
        /// <summary>
        /// Fill Users to comboBoxUser. Needs to be invoked when form loading
        /// </summary>
        private void FillUserCombobox()
        {
            UserList userList = new UserList();
            userList.FillUserComboBox(comboBoxUser);
        }
        /// <summary>
        /// Fill Report Status Combobox. Needs to be invoked when form loading
        /// </summary>
        private void FillReportStatusComboBox()
        {
            ReportStatusList reportStatusList = new ReportStatusList();
            reportStatusList.FillReportStateComboBox(comboBoxReportStatus);
        }
        /// <summary>
        /// This Asnyc method connect to GitHub repository and list all the content
        /// </summary>
        protected async void GetAllContentFromGitHub()
            {
            #region Connect to GitHub Repository and Get All Content 

                if (labelConnStatus.Text == "Not Connected")
                {
                GitHubClient client = new GitHubClient(new ProductHeaderValue("BugTracker"));
                //client.Credentials = new Credentials("1773c8131cfc191ca9d94557895dc7f0eb469a58");
                var basicAuth = new Credentials("metetunca", "metetunca8348");
                client.Credentials = basicAuth;


                var repository = await client.Repository.Get("metetunca", "SampleBugProject");

                labelConnStatus.Text = "Connected To <" + repository.FullName + ">  Language: " +
                                       repository.Language;

                repoUrl = repository.HtmlUrl;

                var contents = await client
                    .Repository
                    .Content
                    .GetAllContents("metetunca", "SampleBugProject");

                listBoxGitHubRepo.DisplayMember = "strText";
                listBoxGitHubRepo.ValueMember = "strValue";
                for (int i = 0; i < contents.Count; i++)
                {
                    ListBoxItem item = new ListBoxItem();
                    item.strText = "Name: " + contents[i].Name + "  " + "Path: " + contents[i].Path + "  " +
                                   "Type: " + contents[i].Type.ToString();
                    item.strValue = contents[i].DownloadUrl.ToString();
                    listBoxGitHubRepo.Items.Add(item);

                }
            }
                else
                {
                MessageBox.Show(this, "Already Connected to Repository", "Warning!", MessageBoxButtons.OK);
                }

                #endregion
        }
        /// <summary>
        /// Open website to see repository connected online
        /// </summary>
        private void GoToReposirotyOnline()
        {
            if (labelConnStatus.Text != "Not Connected")
                System.Diagnostics.Process.Start(repoUrl);
            else
            {
                MessageBox.Show(this,"Connect to Repository","Warning!",MessageBoxButtons.OK);
            }
        }
        /// <summary>
        /// Open website to see selected source code online from repository
        /// </summary>
        private void GoToSourceFileOnline()
        {
            if (labelConnStatus.Text == "Not Connected")
            { MessageBox.Show(this, "Connect to Repository", "Warning!", MessageBoxButtons.OK); }
           else if(GetSelectedGitHubItem()==null)
            {
                MessageBox.Show(this, "Select a File Below", "Warning!", MessageBoxButtons.OK);
            }
            else
            { System.Diagnostics.Process.Start(GetSelectedFileURL(GetSelectedGitHubItem())); }


           
        }
       /// <summary>
       /// To open selected source code in Text Editor Component 
       /// </summary>
        private void OpenSourceCodeViewer()
        {
            _formScViewer = new SourceCodeViewer();

            _formScViewer.fileName = GetSelectedFileName(GetSelectedGitHubItem());
            _formScViewer.Show();

            _formScViewer.FormClosed += new FormClosedEventHandler(frmViewer_FormClosed);
        }
        /// <summary>
        /// Download selected File from Repository
        /// </summary>
        private void DownloadSelectedFile()
        {
            try
            {
                if (listBoxGitHubRepo.SelectedItem != null)
                {

                    SourceFile sFile = new SourceFile();
                    sFile.DownloadUrl = GetSelectedFileURL(GetSelectedGitHubItem());
                    sFile.FileName = GetSelectedFileName(GetSelectedGitHubItem());
                    sFile.DownloadFile();
                    MessageBox.Show("The File Downloaded Successfuly");
                }
                else
                    MessageBox.Show("No File Selected");
            }
            catch { MessageBox.Show("Connection Error"); }
        }
        /// <summary>
        /// Get Selected Line Numbers From Text Editor Component in Closing event
        /// </summary>
        private void GetLineNumbersFromSourceCodeViewer()
        {
            int[] lineNumbers= _formScViewer.SelectedLineNumbers();
            textBoxLineNumberfirst.Text = lineNumbers[0].ToString();
            textBoxLineNumberLast.Text = lineNumbers[1].ToString();
        }

        /// <summary>
        /// Get Selected Filename From listBoxGitHubRepo
        /// </summary>
        /// <param name="selectedGitHubItem"></param>
        /// <returns></returns>
        private string GetSelectedFileName(ListBoxItem selectedGitHubItem)
        {
            string fileName = selectedGitHubItem.strText.Split(' ')[1];
            return fileName;
        }
        /// <summary>
        /// Get Selected File Repository URL
        /// </summary>
        /// <param name="selectedGitHubItem"></param>
        /// <returns></returns>
        private string GetSelectedFileURL(ListBoxItem selectedGitHubItem)
        {
            string selectedValue = selectedGitHubItem.strValue;
            return selectedValue;
        }
        /// <summary>
        /// Get selected GitHub Item.It contaions value and text
        /// </summary>
        /// <returns></returns>
        private ListBoxItem GetSelectedGitHubItem()
        {
            ListBoxItem selectedGitHubItem = listBoxGitHubRepo.SelectedItem as ListBoxItem;
            return selectedGitHubItem;

        }
        /// <summary>
        /// Clear form components
        /// </summary>
        private void ClearFormComponents()
        {
            textBoxBugComment.Text = string.Empty;
            textBoxClassName.Text=String.Empty;
            textBoxLineNumberLast.Text =String.Empty;
            textBoxLineNumberfirst.Text = String.Empty;
            textBoxMethodName.Text = String.Empty;
            textBoxProjectName.Text = String.Empty;
            textBoxSourceFile.Text = String.Empty;
            textBoxTitle.Text = String.Empty;
            comboBoxCodeAuthor.SelectedValue = 0;
            comboBoxReportStatus.SelectedIndex = 0;
            listBoxBugAudit.DataSource = null;
            listBoxBugList.SelectedIndex = -1;
            comboBoxUser.SelectedValue = 0;
        }
        /// <summary>
        /// Check client behaviour before creating new bug report
        /// </summary>
        /// <returns></returns>
        private string checkClientToCreateNewBug()
        {
            string msg = string.Empty;
            if (listBoxBugList.SelectedIndex != -1)
                msg = "Clear Form to Create New Bug Report";
            if (textBoxTitle.Text == string.Empty || Convert.ToInt32(comboBoxUser.SelectedValue) == 0)
                msg = "Title and Last User Should be Filled To Create a New Bug Report";

            return msg;
        }
        
        /// <summary>
        /// Check client behaviour before update a bug
        /// </summary>
        /// <returns></returns>
        private string CheckClientToUpdateBug()
        {
            string msg = string.Empty;
            if (listBoxBugList.SelectedIndex <= -1)
                msg = "Select a Bug From Bug List To Update";


            return msg;
        }
        #endregion
        #region This region contains MainForm Events

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
            InitilizeFormDefaults();
            ShowBugList();
           
        }
        private void buttonGoRepositoryOnline_Click(object sender, EventArgs e)
        {
            GoToReposirotyOnline();
        }
        private void buttonConnectGitHub_Click(object sender, EventArgs e)
        {
            GetAllContentFromGitHub();
        }
        private void listBoxGitHubRepo_DoubleClick(object sender, EventArgs e)
        {
            OpenSourceCodeViewer();
        }
        void frmViewer_FormClosed(object sender, FormClosedEventArgs e)
        {
            GetLineNumbersFromSourceCodeViewer();
        }
        private void buttonDownloadSelectedFile_Click(object sender, EventArgs e)
        {
            DownloadSelectedFile();

        }

        private void buttonGoSourceFileOnline_Click(object sender, EventArgs e)
        {
            GoToSourceFileOnline();
        }
        private void listBoxGitHubRepo_SelectedIndexChanged(object sender, EventArgs e)
        {

            textBoxSourceFile.Text = GetSelectedFileName(GetSelectedGitHubItem());

        }
        private void buttonAddBug_Click(object sender, EventArgs e)
        {
            string controlMsg = checkClientToCreateNewBug();
            if (controlMsg==string.Empty)
            CreateBugReport();
            else
                MessageBox.Show(this, controlMsg, "Message!", MessageBoxButtons.OK);
        }

        
        private void listBoxBugList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxBugList.SelectedIndex != -1)
            {
                int reportId = Convert.ToInt32(listBoxBugList.SelectedValue);
                FillSelectedBugDetails(reportId);
                FillSelectedBugAuditList(reportId);

            }
        }

        private void listBoxBugAudit_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSelectedBugAuditComment(Convert.ToInt32(listBoxBugAudit.SelectedValue));
        }

        private void buttonClearForm_Click(object sender, EventArgs e)
        {
            ClearFormComponents();
        }
        private void buttonUpdateBug_Click(object sender, EventArgs e)
        {
            string controlMsg = CheckClientToUpdateBug();
            if (controlMsg == string.Empty)
                UpdateBugReportDetails();
            else
                MessageBox.Show(this, controlMsg, "Message!", MessageBoxButtons.OK);



        }

        private void checkBoxComment_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxComment.Checked == true)
                textBoxBugComment.Enabled = true;
            else
                textBoxBugComment.Enabled = false;
        }
        #endregion ClassEvents


    }
}

