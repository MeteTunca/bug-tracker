﻿using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BugTracker.Classes;


namespace BugTracker
{
    public partial class SourceCodeViewer : Form
    {
        public string fileName;
        public SourceCodeViewer()
        {
            InitializeComponent();
        }

        private void SourceCodeViewer_Load(object sender, EventArgs e)
        {
            //Load the Source Code File
            this.Text = fileName;
            textEditorControl1.LoadFile(fileName);
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Gets Selected Line Numbers from Text Editor Control
        /// </summary>
        /// <returns></returns>
        public int[] SelectedLineNumbers()
        {
            SourceFile sFile = new SourceFile();
            return sFile.GetSelectedLineNumbers(textEditorControl1.ActiveTextAreaControl);
        }


    }
}
