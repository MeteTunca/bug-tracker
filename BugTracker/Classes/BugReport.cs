namespace BugTracker.Classes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    /// <summary>
    /// Bug Report Entity Class
    /// </summary>
    [Table("BugReport")]
    public partial class BugReport
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BugReport()
        {
            BugReportAudits = new HashSet<BugReportAudit>();
        }

        public long BugReportId { get; set; }

        public long? SourceFileId { get; set; }

        [Required]
        [StringLength(250)]
        public string Title { get; set; }

        [StringLength(100)]
        public string MethodName { get; set; }

        [StringLength(100)]
        public string ClassName { get; set; }

        [StringLength(100)]
        public string ProjectName { get; set; }

        [StringLength(5)]
        public string LineOfCodeStart { get; set; }

        [StringLength(5)]
        public string LineOfCodeEnd { get; set; }

        public DateTime CreateDate { get; set; }

        public virtual SourceFile SourceFile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BugReportAudit> BugReportAudits { get; set; }
        
        
    }
}
