﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Classes
{
    interface IRepositoryContext
    {
        IDbSet<BugReport> BugReports { get; set; }
        DbSet<BugReportAudit> BugReportAudits { get; set; }
        DbSet<ReportState> ReportStates { get; set; }
        DbSet<SourceFile> SourceFiles { get; set; }
        DbSet<sysdiagram> sysdiagrams { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<UserType> UserTypes { get; set; }
    }
}
