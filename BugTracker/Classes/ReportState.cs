namespace BugTracker.Classes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Linq;
    using System.Windows.Forms;
    /// <summary>
    /// Report State Entity Class
    /// </summary>
    [Table("ReportState")]
    public partial class ReportState
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ReportState()
        {
            BugReportAudits = new HashSet<BugReportAudit>();
        }

        [Key]
        public int StateId { get; set; }

        [StringLength(20)]
        public string State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BugReportAudit> BugReportAudits { get; set; }

        
    }
}
