﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker.Classes
{
    public class BugReportDetails
    {
        public long ReportId { get; set; }
        public string Title { get; set; }
        public string LineOfCodeStart { get; set; }
        public string LineOfCodeEnd { get; set; }
        public string MethodName { get; set; }
        public string ClassName { get; set; }
        public string ProjectName { get; set; }
        public long CodeAuthor { get; set; }
        public string SourceFile { get; set; }
        public long UserId { get; set; }
      
        public int Status { get; set; }
       
        
    }
}
