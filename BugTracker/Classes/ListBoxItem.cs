﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Classes
{
    /// <summary>
    /// To manage Listbox Items
    /// </summary>
    public class ListBoxItem
    {
        
            public string strText;
            public string strValue;

        public string DisplayMember { get; internal set; }

        public override string ToString()
            {
                return this.strText;
            }
        
    }
}
