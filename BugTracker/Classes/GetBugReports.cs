﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace BugTracker.Classes
{
    public class GetBugReports
    {
        private BugTrackerContext _context;
        public GetBugReports(BugTrackerContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get All Bug Reports
        /// </summary>
        /// <returns></returns>
        public IQueryable<BugListResult> GetAllBugReports()
        {
             IQueryable<BugListResult> bugsList = from b in _context.BugReports
                           orderby b.CreateDate descending
                           select new BugListResult
                           {
                               BugReportId = b.BugReportId,
                               Title = (from au in _context.BugReportAudits.Where(x => b.BugReportId == x.BugReportId)
                                        from s in _context.ReportStates.Where(x => au.BugStateId == x.StateId)
                                        orderby au.AuditDate descending
                                        select s.State
                                       ).FirstOrDefault() + " | " +
                                       b.Title + " | " + b.CreateDate
                           };

            return bugsList;
        }
    }
}
