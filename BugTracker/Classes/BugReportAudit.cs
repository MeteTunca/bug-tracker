namespace BugTracker.Classes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    /// <summary>
    /// Bug Report Audit Entity Class
    /// </summary>
    [Table("BugReportAudit")]
    public partial class BugReportAudit
    {
        public long BugReportAuditId { get; set; }

        public long BugReportId { get; set; }

        public long UserId { get; set; }

        public int BugStateId { get; set; }

        public DateTime AuditDate { get; set; }

        [StringLength(500)]
        public string Comment { get; set; }

        public virtual BugReport BugReport { get; set; }

        public virtual ReportState ReportState { get; set; }

        public virtual User User { get; set; }
    }
}
