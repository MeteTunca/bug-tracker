﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker.Classes
{
    public class BugAuditList
    {
        public long BugReportAuditId { get; set; }
        public long UserId { get; set; }
        public int BugStateId { get; set; }
        public DateTime BugAuditDate { get; set; }
        public string Comment { get; set; }
        public string DisplayItem { get; set; }
       
    }
}
