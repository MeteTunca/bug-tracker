﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Classes
{
    public class GetBugReportDetails
    {
        private BugTrackerContext _context;
        public GetBugReportDetails(BugTrackerContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get Report Details based on bugReportId
        /// </summary>
        /// <param name="bugReportId"></param>
        /// <returns></returns>
        public IQueryable<BugReportDetails> GetAllBugReports(long bugReportId)
        {
            IQueryable<BugReportDetails> bugReportDetails = (from b in _context.BugReports
                                                             from a in _context.BugReportAudits.Where(x => b.BugReportId == x.BugReportId)
                                                             from s in _context.ReportStates.Where(x => a.BugStateId == x.StateId)
                                                             from f in _context.SourceFiles.Where(x => b.SourceFileId == x.SourceFileId).DefaultIfEmpty()
                                                             from u in _context.Users.Where(x => f.CodeAuthorId == x.UserId).DefaultIfEmpty()

                                                             where b.BugReportId == bugReportId
                                                             orderby a.AuditDate descending
                                                             select new BugReportDetails
                                                             {
                                                                 ReportId = b.BugReportId,
                                                                 Title = b.Title,
                                                                 LineOfCodeStart = b.LineOfCodeStart,
                                                                 LineOfCodeEnd = b.LineOfCodeEnd,
                                                                 MethodName = b.MethodName,
                                                                 ClassName = b.ClassName,
                                                                 ProjectName = b.ProjectName,
                                                                 CodeAuthor = f == null ? 0 : f.CodeAuthorId,
                                                                 SourceFile = f == null ? string.Empty : f.FileName,
                                                                 Status = a.BugStateId,
                                                                 UserId = a.UserId,

                                                             }).Take(1);

            return bugReportDetails;
        }
    }
}
