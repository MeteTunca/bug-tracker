﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker.Classes
{
    public class ReportStatusList
    {
        public int StateId { get; set; }
        public string State { get; set; }
        /// <summary>
        /// To Fill Report State Combobox
        /// </summary>
        /// <param name="comboBox"></param>
        public void FillReportStateComboBox(ComboBox comboBox)
        {
            using (var context = new BugTrackerContext())
            {
                IQueryable<ReportStatusList> stateList = from s in context.ReportStates

                                                    select new ReportStatusList
                                                    {
                                                        StateId = s.StateId,
                                                        State = s.State,
                                                    };


                comboBox.DisplayMember = "State";
                comboBox.ValueMember = "StateId";
                comboBox.DataSource = stateList.ToList();
            }

        }
    }
}
