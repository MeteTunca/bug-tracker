﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Classes
{
    public class BugListResult
    {
        public string Title { get; set; }
        public long BugReportId { get; set; }
    }
}
