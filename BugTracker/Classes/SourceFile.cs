namespace BugTracker.Classes
{
    using ICSharpCode.TextEditor;
    using ICSharpCode.TextEditor.Document;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Net;
    /// <summary>
    /// Source File Entity Class
    /// </summary>
    [Table("SourceFile")]
    public partial class SourceFile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SourceFile()
        {
            BugReports = new HashSet<BugReport>();
        }
        
        public long SourceFileId { get; set; }

        [Required]
        [StringLength(50)]
        public string FileName { get; set; }

        public long CodeAuthorId { get; set; }

        [StringLength(250)]
        public string RepositoryUrl { get; set; }

        [StringLength(250)]
        public string DownloadUrl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BugReport> BugReports { get; set; }

        public virtual User User { get; set; }

        public bool DownloadFile()
        {
            try
            {
                using (var webclient = new WebClient())
                {
                    webclient.DownloadFile(DownloadUrl, FileName);
                }
                return true;
            }
            catch
            { return false; }
        }

        public int[] GetSelectedLineNumbers(TextAreaControl activeTextControl)
        {

            // access the current text area.
            TextAreaControl textAreaControl = activeTextControl;
            int[] result = new int[2];
            // Make sure something's selected
            if (textAreaControl.SelectionManager.HasSomethingSelected)
            {
                // Get the selection
                ISelection currentSelection = textAreaControl.SelectionManager.SelectionCollection[0];

                result[0] = currentSelection.StartPosition.Line + 1;
                result[1] = currentSelection.EndPosition.Line + 1;

            }
            return result;
        }
    }
}
