namespace BugTracker.Classes
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    /// <summary>
    /// Entities for Database Operations
    /// </summary>
    public partial class BugTrackerContext : DbContext
    {
        public BugTrackerContext()
            : base("name=BugTrackerContent")
        {
        }

        public virtual IDbSet<BugReport> BugReports { get; set; }
        public IDbSet<BugReportAudit> BugReportAudits { get; set; }
        public virtual DbSet<ReportState> ReportStates { get; set; }
        public virtual DbSet<SourceFile> SourceFiles { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BugReport>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<BugReport>()
                .Property(e => e.MethodName)
                .IsUnicode(false);

            modelBuilder.Entity<BugReport>()
                .Property(e => e.ClassName)
                .IsUnicode(false);

            modelBuilder.Entity<BugReport>()
                .Property(e => e.ProjectName)
                .IsUnicode(false);

            modelBuilder.Entity<BugReport>()
                .Property(e => e.LineOfCodeStart)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BugReport>()
                .Property(e => e.LineOfCodeEnd)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<BugReport>()
                .HasMany(e => e.BugReportAudits)
                .WithRequired(e => e.BugReport)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BugReportAudit>()
                .Property(e => e.Comment)
                .IsUnicode(false);

            modelBuilder.Entity<ReportState>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<ReportState>()
                .HasMany(e => e.BugReportAudits)
                .WithRequired(e => e.ReportState)
                .HasForeignKey(e => e.BugStateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SourceFile>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<SourceFile>()
                .Property(e => e.RepositoryUrl)
                .IsUnicode(false);

            modelBuilder.Entity<SourceFile>()
                .Property(e => e.DownloadUrl)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Firstname)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Lastname)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BugReportAudits)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.SourceFiles)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CodeAuthorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserType>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.UserType)
                .WillCascadeOnDelete(false);
        }
    }
}
