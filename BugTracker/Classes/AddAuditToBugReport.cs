﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Validation;
namespace BugTracker.Classes
{
    /// <summary>
    /// To Add Audit to Existing Bug Report
    /// </summary>
    public class AddAuditToBugReport
    {
        public BugReport BugReport { get; set; }
        public BugReportAudit BugReportAudit { get; set; }
        /// <summary>
        /// Save Bug Audit Changes to DB
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            try
            {
                using (var db = new BugTrackerContext())
                {
                    
                    db.BugReportAudits.Add(BugReportAudit);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                            ve.PropertyName,
                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                            ve.ErrorMessage);
                    }
}
                
                throw;
            }
        }
    }
}
