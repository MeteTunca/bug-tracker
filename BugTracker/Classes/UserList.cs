﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugTracker.Classes
{
    public class UserList
    {
        public long UserId { get; set; }
        public string FullName { get; set; }
        /// <summary>
        /// To Fill User Combobox
        /// </summary>
        /// <param name="comboBox"></param>
        public void FillUserComboBox(ComboBox comboBox)
        {
            using (var context = new BugTrackerContext())
            {
                IQueryable<UserList> userList = from u in context.Users

                                            select new UserList
                                            {
                                                UserId = u.UserId,
                                                FullName = u.Firstname + " " + u.Lastname,
                                            };
                UserList blankuser=new UserList();
                blankuser.UserId = 0;
                blankuser.FullName = string.Empty;
                List<UserList> list = userList.ToList();
                list.Add(blankuser);
                comboBox.DisplayMember = "FullName";
                comboBox.ValueMember = "UserId";
                comboBox.DataSource = list;
                comboBox.SelectedValue = 0;

            }
            
        }
    }
}
