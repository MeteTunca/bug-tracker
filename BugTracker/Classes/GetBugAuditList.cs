﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Classes
{
    public class GetBugAuditList
    {
        private BugTrackerContext _context;
        public GetBugAuditList(BugTrackerContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get Audit List of the Bug Report based on bugReportId
        /// </summary>
        /// <param name="bugReportId"></param>
        /// <returns></returns>
        public IQueryable<BugAuditList> GetAllBugReportAuditLists(long bugReportId)
        {
            IQueryable<BugAuditList> bugAuditList = from b in _context.BugReportAudits

                                                    join u in _context.Users on b.UserId equals u.UserId
                                                    join t in _context.UserTypes on u.UserTypeId equals t.UserTypeId
                                                    join s in _context.ReportStates on b.BugStateId equals s.StateId
                                                    where b.BugReportId == bugReportId
                                                    orderby b.AuditDate descending
                                                    select new BugAuditList
                                                    {

                                                        BugReportAuditId = b.BugReportAuditId,
                                                        DisplayItem = s.State + " | " + u.Firstname + " " + u.Lastname + "/" + t.Type + " | " + b.AuditDate,
                                                    };

            return bugAuditList;
        }
    }
}
