﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugTracker.Classes
{
    public class GetBugAuditComment
    {
        private BugTrackerContext _context;
        public GetBugAuditComment(BugTrackerContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get Comments of The Audit based on bugReportAuditId
        /// </summary>
        /// <param name="bugReportAuditId"></param>
        /// <returns></returns>
        public IQueryable<BugAuditList> GetBugAuditComments(int bugReportAuditId)
        {
            IQueryable<BugAuditList> bugAuditComment = from b in _context.BugReportAudits

                                                       where b.BugReportAuditId == bugReportAuditId
                                                       orderby b.AuditDate descending
                                                       select new BugAuditList
                                                       {

                                                           Comment = b.Comment,

                                                       };

            return bugAuditComment;
        }
    }
}
