﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Octokit;
using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Document;
using System.Net;

namespace BugTracker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        protected async void Form1_Load(object sender, EventArgs e)
        {
            var client = new GitHubClient(new ProductHeaderValue("BugTracker"));
            //client.Credentials = new Credentials("1773c8131cfc191ca9d94557895dc7f0eb469a58");
            var basicAuth = new Credentials("metetunca", "metetunca8348"); // NOTE: not real credentials
            client.Credentials = basicAuth;

            //var user = await client.User.Get("metetunca");
            //Console.WriteLine("{0} has {1} public repositories - go check out their profile at {2}",
            //    user.Name,
            //    user.PublicRepos,
            //    user.Url);

            //var user = await client.User.Current();

            //var issues = await client.Issue.GetAllForOwnedAndMemberRepositories();
            //var issuesForOctokit = await client.Issue.GetAllForRepository("metetunca", "SampleBugProject");

            // Or we can restrict the search to a specific repo
            //var request = new SearchCodeRequest("Cycle", "metetunca", "SampleBugProject");
            //var result = await client.Search.SearchCode(request);

            var repos = client.Repository.GetAllForCurrent();
            var contents = await client
                .Repository
                .Content
                .GetAllContents("metetunca", "SampleBugProject");
            var repository = await client.Repository.Get("metetunca", "SampleBugProject");

            using (var webclient = new WebClient())
            {
                webclient.DownloadFile("https://raw.githubusercontent.com/metetunca/SampleBugProject/master/Award.cs", "Award.cs");
            }


            textEditorControl1.LoadFile("Award.cs");


            }

        private void button1_Click(object sender, EventArgs e)
        {
            //Text editor selected Line numbers
            // access the current text area.
            TextAreaControl textAreaControl = textEditorControl1.ActiveTextAreaControl;

            // Make sure something's selected
            if (textAreaControl.SelectionManager.HasSomethingSelected)
            {
                // Get the (first) selection
                ISelection currentSelection = textAreaControl.SelectionManager.SelectionCollection[0];

                // get the selected text.
                string selectedText = currentSelection.SelectedText;

                int startline = currentSelection.StartPosition.Line+1;
                int endline = currentSelection.EndPosition.Line+1;
            }
        }
    }
}
